# Python environment

## Python using virtualenv

Based on python 3.11, one can setup a virtual environments. 

```commandline
$ pip3 -V
pip 23.0.1 from /usr/lib/python3/dist-packages/pip (python 3.11)
```

A virtual environment can be created in order to use this module: 

```commandline
$ python3 -m venv ci-collate.venv
$ source ci-collate.venv/bin/activate
```

To reproduce the same virtual environment that this tools is being tested:

```commandline
$ pip install -r requirements.txt
```

To update the packages in the requirements:

```commandline
$ pip install --upgrade pip
$ python -m pip install gitpython python-gitlab pyzstd ruamel.yaml tenacity
$ pip list
Package            Version
------------------ ---------
certifi            2023.11.17
charset-normalizer 3.3.2
gitdb              4.0.11
GitPython          3.1.41
idna               3.6
pip                23.3.2
python-gitlab      4.4.0
pyzstd             0.15.9
requests           2.31.0
requests-toolbelt  1.0.0
ruamel.yaml        0.18.5
ruamel.yaml.clib   0.2.8
setuptools         66.1.1
smmap              5.0.1
tenacity           8.2.3
urllib3            2.2.0
$ pip freeze --all > requirements.txt
```

### running tests

To run the pytests, one extra dependency is needed that is under development

```commandline
$ pip install pytest 
$ pip install git+https://gitlab.freedesktop.org/sergi/python-gitlab-mock 
$ pip install -e . 
```

Then, one can call pytest:

```commandline
$ pytest
```
