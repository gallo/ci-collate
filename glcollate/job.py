#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from .artifact import Artifact
from collections import defaultdict
from .expectations import ExpectationsFiles, ExpectationsProcessor, is_build_stage
from functools import cache
from gitlab.exceptions import GitlabGetError
from gitlab.v4.objects.jobs import ProjectJob
from .logging import Logger, CollateLogger, ERROR
from re import match as re_match
from re import search as re_search
from .repository import Repository
import requests
from tempfile import TemporaryDirectory
from tenacity import retry, stop_after_attempt, wait_exponential, after_log
from typing import Dict, Union
from zipfile import ZipFile


class CollateJob(Logger):
    # TODO: document methods
    __gl_job: Dict[int, ProjectJob] = None
    __project_name: str = None
    __latest_try: int = None
    __expectation_files_path: str = None
    __expectation_files_prefix: str = None
    __expectation_changes: dict = None
    __duplicated_results: dict = None

    def __init__(self, gitlab_job: ProjectJob, project_name: str, *args, **kwargs):
        """
        Wrapper to a gitlab job with methods to manage the access to its trace
        and the artifacts.
        :param gitlab_job: gitlab job object to wrap
        :param project_name:
        """
        super(CollateJob, self).__init__(*args, **kwargs)
        self.__gl_job = {gitlab_job.id: gitlab_job}
        self.__project_name = project_name
        self.__latest_try = gitlab_job.id
        self.__expectation_changes = defaultdict(set)
        self.__duplicated_results = defaultdict(set)
        # self.debug(f"Build CollateJob for {self.name} ({self.id})")

    def __str__(self):
        return self.name

    def __repr__(self):
        return f"CollateJob({self})"

    @property
    def name(self):
        return self.__gl_job[self.__latest_try].name

    @property
    def root_name(self) -> str:
        return self.__root_job_name()

    @property
    def id(self):
        return self.__gl_job[self.__latest_try].id

    @property
    def web_url(self):
        return self.__gl_job[self.__latest_try].web_url

    @property
    def stage(self):
        return self.__gl_job[self.__latest_try].stage

    @property
    def is_from_build_stage(self):
        return is_build_stage(
            self.__project_name, self.__gl_job[self.__latest_try].stage
        )

    @property
    def status(self):
        return self.__gl_job[self.__latest_try].status

    @property
    def attributes(self):
        return self.__gl_job[self.__latest_try].attributes

    @property
    def ids(self):
        ids = list(self.__gl_job.keys())
        ids.sort()
        return ids

    @property
    def has_retries(self) -> bool:
        return len(self.__gl_job) > 1

    @property
    def has_expectation_changes(self) -> bool:
        """
        Boolean to know if there are expectations to change in the job.
        :return: True when there are expectations to update
        """
        return len(self.__expectation_changes) > 0

    @property
    def is_known_where_to_change_expectations(self):
        return (
            self.__expectation_files_path is not None
            and self.__expectation_files_prefix is not None
        )

    @property
    def expectation_changes(self) -> dict:
        """
        Dictionary with the expectations that are recommended to change in the job tests.
        :return: Dictionary with the test result and the tests that finished with this result.
        """
        return dict(self.__expectation_changes)

    @property
    def expectations_path(self):
        return self.__expectation_files_path

    @property
    def expectations_prefix(self):
        return self.__expectation_files_prefix

    def append_retry_job(self, job: ProjectJob) -> None:
        self.__gl_job[job.id] = job
        self.__latest_try = max(self.__gl_job.keys())

    def trace(self, job_id: int = None) -> str:
        """
        Query the traces log of the job.
        :param job_id:
        :return: string with the job trace log
        """
        if job_id is not None and job_id not in self.ids:
            raise KeyError
        if trace := self.__get_trace(job_id):
            return trace
        raise FileNotFoundError("Trace not found")

    # TODO: list the available artifacts

    def get_artifact(self, artifact_name: str, job_id: int = None) -> Artifact:
        """
        Query the artifact file of the job.
        :param artifact_name:
        :param job_id:
        :return: string with the content of the file interpreted as string
        """
        header_msg = f"Get artifact {artifact_name} for {self.name} ({job_id=})"
        if job_id is not None and job_id not in self.ids:
            exception_msg = f"Job {job_id} is not in {self.ids}"
            self.debug(f"{header_msg}: {exception_msg}")
            raise KeyError(exception_msg)
        job_id = self.__latest_try if job_id is None else job_id
        answer = self.__get_artifact(artifact_name, job_id)
        if isinstance(answer, Artifact):
            return answer
        elif isinstance(answer, Exception):
            if isinstance(answer, GitlabGetError) and answer.response_code == 404:
                return Artifact(
                    artifact_name,
                    exception=FileNotFoundError(f"Artifact {artifact_name} not found"),
                )
            return Artifact(artifact_name, exception=answer)

    def expectations_update(self, repo: Repository, path: str, file_prefix: str):
        """

        :param repo:
        :param path:
        :param file_prefix:
        :return:
        """
        # TODO: when the jobs support to report in the artifacts where their expectations are, this transfer of
        #  information will no longer be necessary.
        self.__expectation_files_path = path
        self.__expectation_files_prefix = file_prefix
        job_retries = len(self.ids)
        retries_str = f"{job_retries} runs" if job_retries > 1 else "1 run"
        self.debug(f"Job {self.name} has {retries_str}")
        for job_execution_id in self.ids:
            self.debug(f"Get {self.name} failures artifact for job {job_execution_id}")
            for artifact_name in [
                "*/results.csv.zst",
                "*/failures.csv",
            ]:
                artifact = self.get_artifact(artifact_name, job_execution_id)
                if not artifact.has_content:
                    self.warning(
                        f"Ignore job {job_execution_id} from {self.name} as it didn't "
                        f"generate artifact {artifact_name}."
                    )
                else:
                    content = artifact.content_as_str
                    if len(content) > 100:
                        content = f"{content[:100]}..."
                    self.info(
                        f"Append to {self.name} from execution {job_execution_id} "
                        f"{artifact.file_name}: {content!r}"
                    )
                    self.__append_expectation_changes(
                        artifact.content_as_str.splitlines()
                    )
        summary = []
        for test_result, test_list in self.__expectation_changes.items():
            summary.append(f"{test_result}: {len(test_list)}")
        self.debug(f"Job {self.name} results {' ,'.join(summary)}")
        if self.__duplicated_results:
            self.warning(f"Job {self.name} have different results ({job_retries} runs) on {len(self.__duplicated_results.keys())} tests")
        expectations: ExpectationsFiles = repo.get_expectations_contents(
            self.__expectation_files_path, self.__expectation_files_prefix
        )
        self.__review_expectation_changes_with_duplicated_results()
        ExpectationsProcessor(
            self.name, expectations, self.__expectation_changes
        ).process_expectation_changes()
        repo.apply_expectation_changes(expectations)

    @staticmethod
    def __fail_tests_and_their_causes(fails_contents: list) -> dict:
        """
        From the list of lines in the fails file, convert it to a dictionary where
        the key is the test name, and the value is the test results and the line.
        We can later check if the test has failed for the same reason or another.
        :param fails_contents:
        :return:
        """
        dct = {}
        for i, line in enumerate(fails_contents, start=1):
            if line.startswith("#") or line.count(",") == 0:
                continue  # ignore these lines
            test_name, test_result = line.strip().rsplit(",", 1)
            dct[test_name] = {"result": test_result, "line": i}
        return dct

    @cache
    @retry(
        stop=stop_after_attempt(6),
        wait=wait_exponential(multiplier=1),
        after=after_log(CollateLogger(), ERROR),
    )
    # retry waiting 1, 2, 4, 8, 16, 32 = 63s
    def __get_trace(self, job_id: int = None) -> Union[str, None]:
        try:
            job_id = self.__latest_try if job_id is None else job_id
            return self.__gl_job[job_id].trace().decode("UTF-8")
        except GitlabGetError as exception:
            if exception.response_code == 404:
                return None
            raise exception

    @cache
    @retry(
        stop=stop_after_attempt(6),
        wait=wait_exponential(multiplier=1),
        after=after_log(CollateLogger(), ERROR),
    )
    # retry waiting 1, 2, 4, 8, 16, 32 = 63s
    def __get_artifact(
        self, artifact_pattern: str, job_id: int
    ) -> Union[Artifact, Exception]:
        if artifact_pattern.startswith("*/"):
            try:
                return self.__walk_artifacts(artifact_pattern[2:], job_id)
            except FileNotFoundError as exception:
                return exception
        else:
            try:
                artifact_content = self.__gl_job[job_id].artifact(artifact_pattern)
            except GitlabGetError as exception:
                code = exception.response_code
                if code == 404:
                    # It means the artifact doesn´t exist.
                    # go to check the next path with the generator
                    return exception
                raise exception
            except requests.exceptions.ChunkedEncodingError as exception:
                # When the artifact doesn't exist, it was returning a
                # GitlabGetError with a 404 (Page Not Found). But it seems
                # newer release of gitlab raises a different exception.
                return GitlabGetError(response_code=404)
            else:
                return Artifact(artifact_pattern, content=artifact_content)

    def __root_job_name(self) -> str:
        """
        Remove the suffix on the sharded jobs to have a single name for all of them.
        :return: original name before sharding
        """
        name = self.name
        search = re_match(r"(.*) [0-9]+/[0-9]+$", name)
        if search:  # if it has a " n/m" at the end, remove it.
            name = search.group(1)
        search = re_match(r"(.*)[-_]full$", name)
        if search:  # remove the 'full' suffix
            name = search.group(1)
        return name

    def __walk_artifacts(self, artifact_name: str, job_id: int) -> Artifact:
        """
        Used to aggregate the content of the files in the artifacts of a job. First
        downloading all the artifacts, and then iterate the files in the packet to
        get the content.
        :param artifact_name:
        :param job_id:
        :return:
        """
        # TODO: evaluate if Artifact object could manage to store multiple files
        #  allow to work with them as if they are a single file, but also to
        #  distinguish what comes from each source.
        job = self.__gl_job[job_id]
        tmp = TemporaryDirectory()
        artifacts_zip_name = f"{tmp.name}/artifacts.zip"
        try:
            with open(artifacts_zip_name, "wb") as f:
                job.artifacts(streamed=True, action=f.write)
        except Exception as exception:
            raise FileNotFoundError(f"Artifact {artifact_name} not found")
        self.info(f"Downloaded {self.name} artifacts to {artifacts_zip_name}")
        artifacts_zip = ZipFile(artifacts_zip_name)
        artifact_content = b""
        artifact_found = False
        for element in artifacts_zip.filelist:
            if element.filename.count(artifact_name):
                self.info(f"Artifact found: {element.filename}")
                # Concatenate the content for all the artifact_name files
                artifact_content += artifacts_zip.read(element.filename)
                artifact_found = True
        if not artifact_found:
            raise FileNotFoundError(f"Artifact {artifact_name} not found")
        return Artifact(artifact_name, content=artifact_content)

    def __append_expectation_changes(self, tests: list) -> None:
        """
        From a list of test results record, store them in a dictionary where the key is the test result and the items
        are the tests under that result.
        :param tests: list of test records
        :return:
        """
        for test_record in tests:
            if test_record.startswith("#") or len(test_record) == 0:
                continue  # ignore comment and empty lines
            if test_record.count(",") == 1:
                name, status = test_record.split(",")
            elif test_record.count(",") == 2:
                name, status, _ = test_record.split(",")
            else:
                raise AssertionError(f"Cannot interpret the record {test_record!r}")

            for existing_status, tests_set in self.__expectation_changes.items():
                if existing_status != status and name in tests_set:
                    self.debug(f"Test {name} found in {existing_status} but adding to {status}")
                    self.__duplicated_results[name].add(existing_status)
                    self.__duplicated_results[name].add(status)
            self.__expectation_changes[status].add(name)

    def __review_expectation_changes_with_duplicated_results(self):
        self.warning(f"Found {len(self.__duplicated_results)} retry flakes")
        for test_name, flake_results in self.__duplicated_results.items():
            for test_result in flake_results:
                self.__expectation_changes[test_result].remove(test_name)
            for test_result in ["Flake", "Crash", "Fail"]:
                if test_result in flake_results:
                    self.__expectation_changes[test_result].add(test_name)
            if flake_results.difference({"Flake", "Crash", "Fail", "Pass"}):
                self.warning(f"Flake pair {flake_results} found and not handled.")
