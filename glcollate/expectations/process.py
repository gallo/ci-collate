#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"


from ..logging import Logger
from .files import ExpectationsFiles
from re import match as re_match


class ExpectationsProcessor(Logger):
    __name: str = None
    __expectations: ExpectationsFiles = None
    __expectation_changes: dict = None
    __test_categories: list = None

    def __init__(
        self,
        name: str,
        expectations: ExpectationsFiles,
        expectation_changes: dict,
        *args,
        **kwargs,
    ):
        super(ExpectationsProcessor, self).__init__(*args, **kwargs)
        self.__name = name
        self.__expectations = expectations
        self.info(
            f"For job {self.__name} current expectations: "
            f"{len(self.__expectations.fails.lines)} fail records, "
            f"{len(self.__expectations.flakes.lines)} flake records, "
            f"{len(self.__expectations.skips.lines)} skip records."
        )
        self.__expectation_changes = expectation_changes
        self.__test_categories = list(self.__expectation_changes.keys())

    def process_expectation_changes(self) -> None:
        self.__process_unexpected_pass()
        self.__process_timeout()
        self.__process_flake()
        self.__process_fail_crash()
        self.__process_missing()
        self.__review_unmanaged()

    # first descendant level

    def __process_unexpected_pass(self) -> None:
        for test_category in [
            "UnexpectedPass",
            "UnexpectedImprovement(Pass)",
            "UnexpectedImprovement(Skip)",
        ]:
            if test_category in self.__test_categories:
                self.info(
                    f"There are {test_category} to be removed from {self.__expectations.fails.file_name}"
                )
                tests_records = self.__get_expectation_records(test_category)
                self.__expectations.fails.remove_records(tests_records)
                self.__mark_category_as_processed(test_category)

    def __process_timeout(self) -> None:
        if "Timeout" in self.__test_categories:
            self.info(
                f"There are Timeout tests to be recorded in {self.__expectations.skips.file_name}. "
                f"Also check if they are in {self.__expectations.fails.file_name} or "
                f"in {self.__expectations.flakes.file_name}."
            )
            test_records = self.__get_expectation_records("Timeout")
            # self.debug(f"\t{test_records=}")
            self.__expectations.fails.remove_records(test_records)
            self.__expectations.flakes.remove_records(test_records)
            self.__expectations.skips.add_record(test_records)
            self.__mark_category_as_processed("Timeout")
            # self.debug(
            #     f"{self.__expectations.skips.file_name}:\n{self.__expectations.skips.lines}"
            # )

    def __process_flake(self) -> None:
        if "Flake" in self.__test_categories:
            self.info(
                f"There are flakes to be recorded in {self.__expectations.flakes.file_name}. "
                f"Also check if they are in {self.__expectations.fails.file_name} or "
                f"in {self.__expectations.skips.file_name}."
            )
            tests_records = self.__get_expectation_records("Flake")
            self.__expectations.fails.remove_records(tests_records)
            self.__expectations.flakes.add_record(tests_records)
            self.__expectations.skips.remove_records(tests_records)
            self.__mark_category_as_processed("Flake")

    def __process_fail_crash(self) -> None:
        for test_result in ["Fail", "UnexpectedImprovement(Fail)", "Crash"]:
            if test_result in self.__test_categories:
                self.info(
                    f"There are {test_result} tests to be recorded in {self.__expectations.flakes.file_name}. "
                    f"Also check if they exist with a different test result."
                )
                tests_records = self.__get_expectation_records(test_result)
                self.__expectations.fails.remove_records(tests_records)
                if match := re_match(r"UnexpectedImprovement\((.*)\)", test_result):
                    self.__expectations.fails.add_record(tests_records, status=match.group(1))
                else:
                    self.__expectations.fails.add_record(tests_records, status=test_result)
                self.__mark_category_as_processed(test_result)

    def __process_missing(self) -> None:
        if "Missing" in self.__test_categories:
            self.warning(
                f"There are Missing tests, but this tool don't know (yet) how to proceed with them."
            )
            self.__mark_category_as_processed("Missing")

    def __review_unmanaged(self) -> None:
        for test_result in ["Pass", "Skip", "Warn", "ExpectedFail", "KnownFlake"]:
            if test_result in self.__test_categories:
                self.debug(f"Nothing to do with the tests in {test_result} result")
                self.__mark_category_as_processed(test_result)
        if len(self.__test_categories) > 0:
            self.warning(f"ALERT: Unmanaged categories: {self.__test_categories}")

    # second descendant level

    def __get_expectation_records(self, category: str) -> set:
        return self.__expectation_changes[category]

    def __mark_category_as_processed(self, category: str) -> None:
        self.__test_categories.pop(self.__test_categories.index(category))
