#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"


from ..logging import Logger
from copy import copy


class ExpectationFile(Logger):
    __file_name: str = None
    __lines: list = None

    def __init__(self, file_name: str, lines: list, *args, **kwargs):
        super(ExpectationFile, self).__init__(*args, **kwargs)
        self.__file_name = file_name
        self.__lines = lines

    @property
    def file_name(self):
        return self.__file_name

    @property
    def lines(self):
        return self.__lines

    def add_record(self, test_records: set, status: str = None) -> None:
        for i, line in enumerate(self.__lines):
            # print(f"{i}: {line}")
            for test in copy(test_records):
                # print(f"\t? {test} ", end='')
                if line.startswith(test):
                    self.info(
                        f"Record {test} already found in {self.file_name}, skip to add it"
                    )
                    # print("found")
                    test_records.remove(test)
                # else:
                #     print("NOT found")
        for test in test_records:
            suffix = f",{status}" if status is not None else ""
            new_record = f"{test}{suffix}\n"
            self.__lines.append(new_record)

    def remove_records(self, test_records: set) -> None:
        remove_lines = []
        # print("\n********")
        for i, line in enumerate(self.__lines):
            # print(f"{i}: {line}")
            for test in test_records:
                # print(f"\t? {test} ", end='')
                if line.startswith(test):
                    self.info(
                        f"Remove record {test} found in {self.file_name} line {i} ('{line}')"
                    )
                    # print("found")
                    if i in remove_lines:
                        self.warning(f"Attempt to remove record {i} already in the removal list for {self.file_name}")
                    else:
                        remove_lines.append(i)
                # else:
                #     print("NOT found")
        remove_lines.sort()  # the removals almost never happen in the sequence of the file.
        remove_lines.reverse()  # start from the end or it will change indexes
        for i in remove_lines:
            self.__lines.pop(i)
        # print("********\n")


class ExpectationsFiles(Logger):
    __fails: ExpectationFile = None
    __flakes: ExpectationFile = None
    __skips: ExpectationFile = None
    __len: int = None

    def __init__(self, *args, **kwargs):
        super(ExpectationsFiles, self).__init__(*args, **kwargs)
        self.__len = 0

    @property
    def fails(self):
        return self.__fails

    @property
    def flakes(self):
        return self.__flakes

    @property
    def skips(self):
        return self.__skips

    def insert(self, name: str, obj: ExpectationFile):
        if name == "fails":
            if not self.__fails:
                self.__len += 1
            self.__fails = obj
        elif name == "flakes":
            if not self.__flakes:
                self.__len += 1
            self.__flakes = obj
        elif name == "skips":
            if not self.__skips:
                self.__len += 1
            self.__skips = obj

    def __iter__(self):
        if self.__fails:
            yield self.__fails
        if self.__flakes:
            yield self.flakes
        if self.__skips:
            yield self.__skips

    def __len__(self):
        return self.__len
