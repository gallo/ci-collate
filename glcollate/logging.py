#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"


from .abstract import Singleton
import logging
from logging import CRITICAL, ERROR, WARNING, INFO, DEBUG
import sys


class CollateLogger(Singleton):
    """
    Singleton to setup a python logger.
    """

    __logger: logging.Logger = None
    __console_handler: logging.Handler = None

    def __init__(self, *args, **kwargs):
        super(CollateLogger, self).__init__(*args, **kwargs)
        if not self.__logger:
            self.__logger = logging.getLogger("ci-collate")
            self.__configure_logging()

    @staticmethod
    def get_available_loggers():
        """
        Provide the list of names for the loggers already build in the current python.
        :return:
        """
        return list(logging.root.manager.loggerDict.keys())

    @property
    def name(self) -> str:
        return self.__logger.name

    def change_logger(self, new_logger_name: str):
        """
        When the tool is used as a library, it may have its own logger. It is
        possible to link this logger to the one used by the tool importing this
        library.
        :param new_logger_name: string from get_available_loggers()
        :return:
        """
        if new_logger_name in self.get_available_loggers():
            self.__logger = logging.getLogger(new_logger_name)

    def __configure_logging(self):
        self.__logger.setLevel(INFO)
        self.__console_handler = logging.StreamHandler(stream=sys.stdout)
        self.__console_handler.formatter = logging.Formatter(
            "%(asctime)s - %(levelname)s - %(message)s"
        )
        self.__console_handler.level = INFO
        self.__logger.addHandler(self.__console_handler)

    def setLevel(self, value: int):
        self.__logger.setLevel(value)
        self.__console_handler.level = value

    def critical(self, *args, **kwargs):
        self.__logger.critical(*args, **kwargs)

    def error(self, *args, **kwargs):
        self.__logger.error(*args, **kwargs)

    def warning(self, *args, **kwargs):
        self.__logger.warning(*args, **kwargs)

    def info(self, *args, **kwargs):
        self.__logger.info(*args, **kwargs)

    def debug(self, *args, **kwargs):
        self.__logger.debug(*args, **kwargs)


class Logger:
    """
    Logger superclass to provide the methods to simplify logging messages.
    It manages the propagation of the messages to a global singleton logger object.
    """

    _logger: CollateLogger

    def __init__(self, *args, **kwargs):
        super(Logger, self).__init__(*args, **kwargs)
        logger = CollateLogger()
        self.critical = logger.critical
        self.error = logger.error
        self.warning = logger.warning
        self.info = logger.info
        self.debug = logger.debug
        self._logger = logger
