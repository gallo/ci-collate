#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

import contextlib

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from collections import defaultdict, deque

import tenacity

from .job import CollateJob
from .expectations import build_expectations_paths, is_build_stage
from gitlab.v4.objects.pipelines import ProjectPipeline
from gitlab.v4.objects.projects import Project
from .logging import Logger
from pprint import pformat
from re import match as re_match
from .repository import Repository
from typing import Union
import concurrent.futures


class CollatePipeline(Logger):
    # TODO: document methods
    __gl_pipeline: ProjectPipeline = None
    __all_jobs: deque = None
    __jobs_by_stage: dict = None
    __gitlab_url: str = None
    __project: Project = None
    __exclude_retried: bool = None
    __jobs_with_retries: list = None
    __unknown_expectations_jobs: list = None
    __possible_trace_jobs: list = None

    def __init__(
        self,
        gitlab_pipeline: ProjectPipeline,
        collate_jobs: list,
        gitlab_url: str,
        project: Project,
        exclude_retried: bool = False,
        *args,
        **kwargs,
    ):
        """
        Represents a pipeline in a gitlab server providing information and an
        interface to collate the results.
        :param gitlab_pipeline: gitlab pipeline object to wrap
        :param collate_jobs: wrapped jobs in the pipeline
        :param exclude_retried
        """
        super(CollatePipeline, self).__init__(*args, **kwargs)
        self.__gl_pipeline = gitlab_pipeline
        self.__all_jobs = deque()
        self.__jobs_by_stage = defaultdict(deque)
        self.__exclude_retried = exclude_retried
        self.__jobs_with_retries = []
        for job in collate_jobs:
            self.debug(
                f"Organizing job {job.name} -> stage:{job.stage} status:{job.status}"
            )
            self.__all_jobs.append(job)
            self.__jobs_by_stage[job.stage].append(job)
            if job.has_retries:
                self.__jobs_with_retries.append(job)
        self.__unknown_expectations_jobs = []
        self.__possible_trace_jobs = []
        self.__gitlab_url = gitlab_url
        self.__project = project
        # self.debug(f"Build CollatePipeline for {self.__gl_pipeline.id}")

    def __str__(self):
        return f"{self.id}"

    def __repr__(self):
        return f"CollatePipeline({self})"

    @property
    def id(self):
        return self.__gl_pipeline.id

    @property
    def web_url(self):
        return self.__gl_pipeline.web_url

    @property
    def status(self):
        return self.__gl_pipeline.status

    @property
    def attributes(self):
        return self.__gl_pipeline.attributes

    @property
    def pipeline_stages(self) -> set:
        """
        Query which stages are in the pipeline.
        :return: Set of stages in the pipeline.
        """
        return set(self.__jobs_by_stage.keys())

    @property
    def job_stata(self) -> set:
        """
        Query about the status that the jobs in the pipeline has.
        :return: Set of job status currently in the pipelin
        """
        return set((job.status for job in self.__all_jobs))

    @property
    def exclude_retried(self) -> bool:
        return self.__exclude_retried

    @exclude_retried.setter
    def exclude_retried(self, value: bool) -> None:
        self.__exclude_retried = value

    @property
    def jobs_with_retries(self) -> set:
        return set(self.__jobs_with_retries)

    def jobs(self, job_regex: str = None) -> set:
        return set(self.__job_selection(job_regex=job_regex).values())

    def jobs_in_stage(self, stage: Union[str, list, set, None]) -> set:
        """
        Query the jobs, filtered by belonging to a stage or a set of them.
        :param stage: a stage or a list of them
        :return: set jobs in the stages in the pipeline
        """
        stage = "*" if stage is None else stage
        return self.__query_dictionary_keys(stage, self.__jobs_by_stage)

    def jobs_in_status(self, status: Union[str, list, set, None] = None) -> set:
        """
        Query the jobs that have the status filtered by the argument.
        :param status: a status or a list of them
        :return: set of jobs currently in the requested status in the pipeline
        """
        status = "*" if status is None else status
        jobs_by_status = defaultdict(deque)
        for job in self.__all_jobs:
            jobs_by_status[job.status].append(job)
        return self.__query_dictionary_keys(status, jobs_by_status)

    def trace(
        self,
        job_regex: str = None,
        status: Union[str, list, set] = None,
        stage: Union[str, list, set] = None,
    ) -> dict:
        """
        Query the trace for a set of jobs. Many arguments allow to subset the
        jobs in the pipeline in many ways.
        :param job_regex: regular expression to filter the jobs in the pipeline
        :param status: status or a list of them to filter the jobs
        :param stage: stage or a list of them to filter the jobs
        :return: dictionary with the trace on each of the jobs
        """
        return self.__job_selection(
            "trace", job_regex=job_regex, status=status, stage=stage
        )

    # TODO: list the available artifacts

    def get_artifact(
        self,
        artifact_name,
        job_regex: str = None,
        status: Union[str, list, set] = None,
        stage: Union[str, list, set] = None,
    ) -> dict:
        """
        Query artifacts for a set of jobs. Many arguments allow to subset the
        jobs in the pipeline in many ways.
        :param artifact_name: string with the name
        :param job_regex: regular expression to filter the jobs in the pipeline
        :param status: status or a list of them to filter the jobs
        :param stage: stage or a list of them to filter the jobs
        :return: dictionary with the artifact on each of the jobs
        """
        # The paths in virglrenderer may have an intermediate subdirectory:
        #  results/{deqp,piglit}-{gl,gles}-{host,virt}/failures.csv
        #  So, the pattern to get them would be:
        #   - results/*/failures.csv -> results/{unsharded_job_name}/failures.csv
        #  But, if not found there, it would also check:
        #   - results/failures.csv
        self.debug(
            f"Request to get artifact {artifact_name!r} to pipeline {self.id} with filters {job_regex=}, {status=}, {stage=}"
        )
        return self.__job_selection(
            "get_artifact",
            args_list=[artifact_name],
            job_regex=job_regex,
            status=status,
            stage=stage,
        )

    def expectations_update(
        self,
        local_clone: str = None,
        branch_namespace: str = None,
        patch_branch: str = None,
        patch_name: str = None,
        with_merge_request: bool = None,
    ) -> None:
        """
        Update the expectation files and generate a patch.
        :param local_clone:
        :param branch_namespace:
        :param patch_branch:
        :param patch_name:
        :param with_merge_request:
        :return:
        """
        repo = Repository(
            gitlab_url=self.__gitlab_url,
            namespace=branch_namespace,
            project=self.__project.name,
            branch=patch_branch,
            clone_path=local_clone,
        )
        repo.clone()
        patch_name = patch_name if patch_name else f"{self.id}_expectations_update"
        repo.create_branch_from_commit(patch_name, self.__gl_pipeline.sha)
        expectation = build_expectations_paths(self.__project.name)
        # self.debug(f"***\n{pformat(expectation)}\n***")
        self.debug(f"Pipeline {self.id} has {len(self.__all_jobs)} jobs to process")
        for job in self.__all_jobs:
            if job.is_from_build_stage:
                self.debug(
                    f"Skip {job.name} as its {job.stage} stage is from the build stages set."
                )
                continue
            if job.status not in ["success", "failed"]:
                self.debug(
                    f"Skip {job.name} as its {job.status} status doesn't provide "
                    f"information for the expectations update"
                )
                continue
            try:
                target = self.__project.name.lower()
                # FIXME: it's horrible:
                if target in ["virglrenderer", "linux"]:
                    expectations_key = job.root_name
                elif target in ["mesa"]:
                    expectations_key = (job.root_name, job.stage)
                else:
                    raise KeyError(f"Unmanaged {target} project")
                # TODO: When the jobs have this information in the artifacts, we will only trigger the
                #  job.expectations_update(repo) method.
                expectations_key_set = set(expectation[expectations_key].keys())
                if expectations_key_set == {"path", "files"}:
                    job_expectations_path = expectation[expectations_key]["path"]
                    job_expectations_prefix = expectation[expectations_key]["files"]
                    job.expectations_update(
                        repo, job_expectations_path, job_expectations_prefix
                    )
                elif expectations_key_set == {"skip"}:
                    skip_reason = expectation[expectations_key]["skip"]
                    self.info(f"Skip {job.name} from expectations update because {skip_reason}")
            except KeyError:
                # FIXME: this is also raised with build jobs (or non-testing jobs)
                #  and this alert may generate alarm-blindness
                self.error(
                    f"Unknown expectations for {job.root_name} in {job.stage} stage"
                )
                if job.name.count("trace"):
                    self.__possible_trace_jobs.append(job.name)
                else:
                    self.__unknown_expectations_jobs.append(job.name)
        if len(self.__possible_trace_jobs) > 0:
            self.warning(f"Summary: Probaby traces jobs: {' ,'.join(self.__possible_trace_jobs)}")
        if len(self.__unknown_expectations_jobs) > 0:
            self.error(f"Summary: Jobs with unknown expectations: {' ,'.join(self.__unknown_expectations_jobs)}")
        with open(f"{patch_name}.patch", "wt") as file_descriptor:
            file_descriptor.writelines(repo.diff())
        # if with_merge_request:
        #     # TODO: prepare a merge request

    def __job_selection(
        self,
        action: str = None,
        args_list: list = None,
        args_dict: dict = None,
        job_regex: str = None,
        status: Union[str, list, set] = "*",
        stage: Union[str, list, set] = "*",
    ) -> dict:
        """
        Filter the jobs in the pipeline by their name using a regular expression
        to then call a specific action for each of them and summarize the
        results with a dictionary structure.
        :param action: string with the name of a job object method
        :param args_list: args to the action
        :param args_dict: kwargs to the action
        :param job_regex: regular expression to filter jobs
        :param status: status of the jobs to include in the action
        :param stage: stage of the jobs to include in the action
        :return: dictionary with the requested information by job
        """
        args_list = [] if args_list is None else args_list
        args_dict = {} if args_dict is None else args_dict
        output = defaultdict(list) if action is None else defaultdict(dict)
        requests_jobs = self.jobs_in_status(status).intersection(
            self.jobs_in_stage(stage)
        )

        jobs_filtered = [
            job for job in requests_jobs if re_match(job_regex or "", job.name)
        ]
        # self.debug(
        #     f"Ready to launch a concurrent request {action!r} (with {args_list=}, {args_dict=}) for {jobs_filtered}"
        # )
        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures = [
                executor.submit(
                    self._do_job_action, action, args_dict, args_list, output, job
                )
                for job in jobs_filtered
            ]
            concurrent.futures.wait(futures)
        return dict(output)

    def _do_job_action(
        self,
        action: str,
        args_dict: dict,
        args_list: list,
        output: defaultdict,
        job: CollateJob,
    ):
        if not action:
            output[job.name] = job
            return

        if not hasattr(job, action):
            self.warning(f"Job {job.name} doesn't have attribute {action}")
            return

        if self.exclude_retried or not job.has_retries:
            # self.debug(
            #     f"Action {action} (with {args_list=}, {args_dict=}) for {job.name} ({job.id})"
            # )
            job_id = args_dict["job_id"] = job.id
            output[job.name][job_id] = self.__call_action(
                getattr(job, action), args_list, args_dict
            )
            return

        for job_id in job.ids:
            # self.debug(
            #     f"Action {action} (with {args_list=}, {args_dict=}) for {job.name} ({job.ids})"
            # )
            # TODO: transform to concurrent with ThreadPoolExecutor
            args_dict["job_id"] = job_id
            output[job.name][job_id] = self.__call_action(
                getattr(job, action), args_list, args_dict
            )
        with contextlib.suppress(KeyError):
            # When multiple threads in different jobs handles the same job.ids list
            # the thread might pop "job_id" when it is already empty, throwing KeyError.
            # Ignore the error in this case, since it was correctly placed in "output"
            # TODO: check how we can avoid handling the same job.id multiple times.
            args_dict.pop("job_id")

    @staticmethod
    def __query_dictionary_keys(query: Union[str, list, set], dct: dict) -> set:
        """
        One would like to subset the jobs by one or moy keys, or even get all
        of them. This method allows to create a single list of elements with
        the jobs satisfying the condition to be in the keys of the query.
        :param query: can be a single name or a set of them
        :param dct: the dictionary from where the values are listed.
        :return: list of elements that satisfies the conditions.
        """
        if isinstance(query, str):
            if query == "*":
                lst = []
                for value in dct.values():
                    lst += list(value)
                return set(lst)
            if query in dct.keys():
                return set(dct[query])
            raise KeyError(f"Review the available keys. {query!r} isn't there.")
        if isinstance(query, list):
            query = set(query)
        if isinstance(query, set):
            available_values = set(dct.keys())
            if not query.issubset(available_values):
                invalid = query.difference(available_values)
                raise KeyError(
                    f"Review the available keys. {invalid} "
                    f"{'is' if len(invalid) == 1 else 'are'}n't "
                    f"there."
                )
            lst = []
            for element in query:
                lst += list(dct[element])
            return set(lst)

    def __call_action(self, caller: callable, args_list: list, args_dict: dict) -> str:
        try:
            return caller(*args_list, **args_dict)
        except FileNotFoundError as exception:
            return f"{exception}"
        except tenacity.RetryError as exception:
            self.warning(f"Tenacity retry error: {exception}")
        except Exception as exception:
            self.error(f"Alert! Unexpected {type(exception)} exception {exception}")
            raise exception
