#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"


from .logging import Logger
from pyzstd import decompress


class Artifact(Logger):
    __file_name: str = None
    __zst: bool = None
    __content: bytes = None
    __exception: Exception = None

    def __init__(
        self,
        file_name: str,
        content: bytes = None,
        exception: Exception = None,
        *args,
        **kwargs
    ):
        super(Artifact, self).__init__(*args, **kwargs)
        self.__file_name = file_name
        if self.__file_name.endswith(".zst"):
            self.__zst = True
        self.__content = content
        self.__exception = exception

    def __repr__(self):
        return self.content_as_str

    @property
    def file_name(self) -> str:
        return self.__file_name

    @property
    def has_content(self):
        return self.__exception is None

    @property
    def exception_as_str(self) -> str:
        return self.__exception.args[0]

    @property
    def content_as_bytes(self) -> bytes:
        if self.__exception:
            return self.__exception.args[0].encode()
        if self.__zst:
            return decompress(self.__content)
        return self.__content

    @property
    def content_as_str(self) -> str:
        if self.__exception:
            return self.__exception.args[0]
        if self.__zst:
            return decompress(self.__content).decode("UTF-8")
        return self.__content.decode("UTF-8")
