#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"

from .defaults import default_project_name
from gitlab.exceptions import GitlabGetError
from gitlab.v4.objects.jobs import ProjectJob
from gitlab.v4.objects.pipelines import ProjectPipeline
from gitlab.v4.objects.projects import Project
from .glproxy import GitlabProxy
from .job import CollateJob
from .logging import Logger, CollateLogger, ERROR
import os
from .pipeline import CollatePipeline
from tenacity import retry, stop_after_attempt, wait_exponential, after_log
from typing import Union
import concurrent.futures


class Collate(Logger):
    __gitlab_url: str = None
    __token_file: str = None

    __gitlab_proxy: GitlabProxy = None
    __project: Project = None

    def __init__(
        self,
        gitlab_url=None,
        gitlab_token_file_name=None,
        namespace=None,
        project=None,
        *args,
        **kwargs,
    ):
        """
        Bind to a gitlab project in a given namespace
        :param gitlab_url: string of the gitlab server url
        :param gitlab_token_file_name: file name where a token is stored
        :param namespace: namespace of the gitlab server where the project is
        :param project: project of the gitlab server to bind
        """
        super(Collate, self).__init__(*args, **kwargs)
        self.__gitlab_proxy = GitlabProxy(gitlab_url, gitlab_token_file_name)
        namespace = (
            self.__gitlab_proxy.user.username if namespace is None else namespace
        )
        project_name = project if project is not None else default_project_name
        self.__build_project_object(namespace, project_name)
        self.debug(f"Collate object build for {self.__project.web_url}")

    @property
    def namespace(self):
        return self.__project.namespace["path"]

    @property
    def project_name(self):
        return self.__project.name

    @property
    def path_with_namespace(self):
        return self.__project.path_with_namespace

    def from_job(self, idn: int) -> CollateJob:
        """
        Get a CollateJob object, wrapper of the gitlab job.
        :param idn: number identifying the job
        :return: job wrapper
        """
        self.debug(f"from_job({idn=})")
        gl_job = self.__get_job_obj(idn)
        self.info(f"Requested CollateJob object for {gl_job.name} ({gl_job.id})")
        if gl_job is None:
            raise AssertionError(
                f"Cannot bind with {idn} job in {self.path_with_namespace} "
                f"on {self.__gitlab_proxy.gitlab_url}"
            )
        return CollateJob(gl_job, self.project_name)

    def from_pipeline(self, idn: int, exclude_retried: bool = False) -> CollatePipeline:
        """
        Get a CollatePipeline object, wrapper of the gitlab pipeline
        :param idn: number identifying the pipeline
        :param exclude_retried: allow to exclude retried jobs
        :return: pipeline wrapper
        """

        def _get_job_to_pipeline(pipeline_jobs, exclude_retried, job):
            gl_job = self.__get_job_obj(job.id)
            if gl_job.name not in pipeline_jobs:
                pipeline_jobs[gl_job.name] = CollateJob(gl_job, self.project_name)
                self.debug(f"Build job {job.name} ({job.id}) from pipeline {idn}")
            elif not exclude_retried:
                pipeline_jobs[gl_job.name].append_retry_job(gl_job)
                self.debug(f"Retried job {job.name} ({job.id}) from pipeline {idn}")

        self.debug(f"from_pipeline({idn=}, {exclude_retried=})")
        gl_pipeline = self.__get_pipeline_obj(idn)
        if gl_pipeline is None:
            raise AssertionError(
                f"Cannot bind with {idn} pipeline in {self.path_with_namespace} "
                f"on {self.__gitlab_proxy.gitlab_url}"
            )
        self.info(f"Requested CollatePipeline for {gl_pipeline.id}")
        pipeline_jobs = {}

        with concurrent.futures.ThreadPoolExecutor() as executor:
            futures = [
                executor.submit(
                    _get_job_to_pipeline, pipeline_jobs, exclude_retried, job
                )
                for job in gl_pipeline.jobs.list(iterator=True, include_retried=True)
            ]
            concurrent.futures.wait(futures)

        return CollatePipeline(
            gl_pipeline,
            list(pipeline_jobs.values()),
            self.__gitlab_proxy.gitlab_url,
            self.__project,
            exclude_retried,
        )

    def __build_project_object(self, namespace: str, project_name: str) -> None:
        """
        Build the path with namespace to the project and bind the gitlab
        project object.
        :param namespace:
        :param project_name:
        :return: None
        """
        path_with_namespace = f"{namespace}/{project_name}"
        self.__project = self.__get_project_obj(path_with_namespace)
        if self.__project is None:
            raise AssertionError(
                f"Cannot bind with {path_with_namespace} "
                f"on {self.__gitlab_proxy.gitlab_url}"
            )

    @retry(
        stop=stop_after_attempt(6),
        wait=wait_exponential(multiplier=1),
        after=after_log(CollateLogger(), ERROR),
    )
    # retry waiting 1, 2, 4, 8, 16, 32 = 63s
    def __get_project_obj(self, path_with_namespace) -> Union[Project, None]:
        try:
            return self.__gitlab_proxy.projects.get(path_with_namespace)
        except GitlabGetError as exception:
            if exception.response_code == 404:
                return None

    @retry(
        stop=stop_after_attempt(6),
        wait=wait_exponential(multiplier=1),
        after=after_log(CollateLogger(), ERROR),
    )
    # retry waiting 1, 2, 4, 8, 16, 32 = 63s
    def __get_job_obj(self, job_id) -> Union[ProjectJob, None]:
        try:
            return self.__project.jobs.get(job_id)
        except GitlabGetError as exception:
            if exception.response_code == 404:
                return None

    @retry(
        stop=stop_after_attempt(6),
        wait=wait_exponential(multiplier=1),
        after=after_log(CollateLogger(), ERROR),
    )
    # retry waiting 1, 2, 4, 8, 16, 32 = 63s
    def __get_pipeline_obj(self, pipeline_id) -> Union[ProjectPipeline, None]:
        try:
            return self.__project.pipelines.get(pipeline_id)
        except GitlabGetError as exception:
            if exception.response_code == 404:
                return None
