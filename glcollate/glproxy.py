#!/usr/bin/env python3.9

# Copyright (C) 2023 Collabora Ltd
#
# Permission is hereby granted, free of charge, to any person obtaining a
# copy of this software and associated documentation files (the "Software"),
# to deal in the Software without restriction, including without limitation
# the rights to use, copy, modify, merge, publish, distribute, sublicense,
# and/or sell copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
# OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
# ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.
#
# SPDX-License-Identifier: MIT

__author__ = "Sergi Blanch Torne"
__email__ = "sergi.blanch.torne@collabora.com"
__copyright__ = "Copyright (C) 2023 Collabora Ltd"


from .defaults import default_gitlab_token_file_name, default_gitlab_url
from gitlab import Gitlab
from gitlab.v4.objects import CurrentUser, ProjectManager
from .logging import Logger, CollateLogger, ERROR
import os
from tenacity import retry, stop_after_attempt, wait_exponential, after_log


class GitlabProxy(Logger):
    def __init__(
        self,
        gitlab_url: str = None,
        gitlab_token_file_name: str = None,
        *args,
        **kwargs,
    ):
        super(GitlabProxy, self).__init__(*args, **kwargs)
        self.__gitlab_url = gitlab_url if gitlab_url is not None else default_gitlab_url
        self.debug(f"GitlabProxy({gitlab_url=}, {gitlab_token_file_name=})")
        self.__build_gitlab_object(gitlab_token_file_name)

    @property
    def gitlab_url(self):
        return self.__gitlab_url

    @property
    def user(self) -> CurrentUser:
        return self.__gl.user

    @property
    def projects(self) -> ProjectManager:
        return self.__gl.projects

    def __build_gitlab_object(self, file_name: str = None) -> None:
        """
        Build the link to the gitlab server to collate information about jobs
        and pipelines
        :return: None
        """
        gitlab_token = None
        if file_name is not None:
            self.debug(f"Using {file_name} to get a gitlab token")
            gitlab_token = self.__get_token_file_content(file_name)
        if gitlab_token is None:
            gitlab_token = os.environ.get("GITLAB_TOKEN")
        if gitlab_token is None:
            self.debug(
                f"Environment variable GITLAB_TOKEN not set, try the default location of a gitlab token file"
            )
            gitlab_token = self.__get_token_file_content(default_gitlab_token_file_name)
        self.__gl = self._gitlab_builder(
            url=self.__gitlab_url, private_token=gitlab_token
        )
        self.__gl.auth()
        self.debug(f"Bind with {self.gitlab_url} with user {self.user.username}")

    def __get_token_file_content(self, name: str) -> str:
        self.__token_file = os.path.expanduser(name)
        try:
            with open(self.__token_file) as file_descriptor:
                return file_descriptor.read().strip()
        except FileNotFoundError:
            raise AssertionError(
                f"Use the GITLAB_TOKEN environment variable or "
                f"the ~/.gitlab-token to provide this information"
            )

    @staticmethod
    @retry(
        stop=stop_after_attempt(6),
        wait=wait_exponential(multiplier=1),
        after=after_log(CollateLogger(), ERROR),
    )
    # retry waiting 1, 2, 4, 8, 16, 32 = 63s
    def _gitlab_builder(url, private_token=None) -> Gitlab:
        return Gitlab(url, private_token=private_token, retry_transient_errors=True)
